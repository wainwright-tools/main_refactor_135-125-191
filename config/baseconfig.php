    <?php
// config for Respins/BaseFunctions
return [

    'server_ip' => '81.24.11.236',

    'access' => [
        'url' => env('APP_URL'),
        'whitelist_ips' => '81.24.11.236@cloudfare,109.37.159.108@cloudflare,249.141.128.33@cloudflare', // whitelist ips for access to the app in format "ip:type", type is either direct or cloudflare
    ],
    
    'mock' => [
        'starting_balance' => 10000, //integer in cents, "100$" balance would be 10000
        'callback_url' => 'https://gate.do/r',
    ],

    'caching' => [ // Caching length is in seconds, most caching is used within DataController. Advise is to set most caching options to 300 (5 minutes).
        'length_getWhitelistIPs' => 60,
        'length_getProvider' => 0,
        'length_getGames' => 0,
    ],

    'proxies' => [
        '1' => 'https://u140512p136552.web0121.zxcs-klant.nl/api/proxy/direct',
    ],

    'frontend' => [
        'theme' => 'livewire', // Set to 'default' or if you use wave set to 'wave'
        'include' => "@extends('theme::layouts.app')",
        'launcher_url' => env('APP_URL', 'localhost'),
        'launcher_path' => 'web/respins.io/games/launch',
        'gameslist' => [
            'games_per_page' => '36',
            'show_provider_nav' => true,
            'show_header' => true,
            'show_extended_gameinfo' => true,
            'show_object' => true,
        ],
    ],

    'host' => [
    	
    ],

    'game_config' => [
        
    ],

];
