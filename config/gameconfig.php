<?php
// Config for internal game (grey) handling
return [

    'session_entry_url' => env('APP_URL').'/web/respins.io/entrySession',
    'thumbnail_wide' => 'https://cdn2.softswiss.net/arlekincasino/i/s2/',
    'thumbnail_square' => 'https://cdn2.softswiss.net/arlekincasino/i/s4/',

    'bgaming' => [
        'new_api_endpoint' => env('APP_URL').'/api/respins.io/games/bgaming/',
        'controller' => \Respins\BaseFunctions\Controllers\Game\BgamingController::class,
        'extra_game_metadata' => 0,
        'launcher_behaviour' => 'internal_game', // 'internal_game' or 'redirect' - expecting url on 'redirect' on controller::requestSession()
    ],
    'pragmaticplay' => [
        'new_api_endpoint' => env('APP_URL').'/api/respins.io/games/bgaming/',
        'controller' => \Respins\BaseFunctions\Controllers\Game\PragmaticPlayController::class,
        'extra_game_metadata' => \Respins\BaseFunctions\Controllers\Game\PragmaticPlayController::class,
        'launcher_behaviour' => 'internal_game', // 'internal_game' or 'redirect' - expecting url on 'redirect' on controller::requestSession()
    ],

    'evoplay_disabled' => [
        'controller' => \Respins\BaseFunctions\Controllers\Game\EvoplayController::class,
        'launcher_behaviour' => 'redirect', // 'internal_game' or 'redirect' - expecting url on 'redirect' on controller::requestSession()
        'endpoint' => 'https://u140512p136552.web0121.zxcs-klant.nl/api/games/evoplay',
        'key' => 'c62ac7a6018ab31c1e9daac4532228dc72203ae5407ceb8b9c38fb8d07ad33c8c974c77d2f2e5ca5fb391a3e81e5981a30da849ab7a4f16547905bb416e734388bc403345461b071d141c79ff8b44ad40a6d43b16070285ab827025d364d9bca62e30b0414599eabc4a31fc6c481184db15c3d92a70db7bb5b63ef4c13f65a32',    
    ],

    'api_settings' => [
        'signature_password' => 'sig22nature559',
    ],

];
