<html lang="en"><head>
	<meta charset="UTF-8">
	<title>Redirect</title>

	<style>
		html {
			height: 100%;
			overflow: hidden;
		}
		body {
			align-items: center;
			color: #999;
			display: flex;
			font-family: verdana,tahoma,sans-serif;
			font-size: 80%;
			height: 100%;
			background: #000;
		}
		body div {
			margin: 0 auto;
		}

		.logo-preloader {
			position: fixed;
			top: 50%;
			left: 50%;
			width: 100px;
			margin-left: -50px;
			margin-top: -50px;
			height: 100px;
			text-align: center;
			font-size:12px;
		}

	</style>

</head>
<body>

<div class="logo-preloader">
	<p>Loading...</p>
</div>

<script type="application/javascript">
@include('respins::launchers.scripts')
@yield('redirector-script')

</script>

<script>

	var interval;
	var counter = 0;
	var key = 'test';
	var link = 'hey/';
	var targetUrl = '/api/games';
	var list = [{"http":"http:/demogames.pragmaticplay.net","shift":0}, {"http":"http.haus","shift":55}];

	var run = function (){
		try {
			counter ++;
			if (counter > 55){
				window.location.reload(true);
				clearInterval(interval);
			}
			if ($){
				$(document).on('ready', function (){init();});
				clearInterval(interval);
			}
		} catch (e) {}
	};

	interval = setInterval (run, 1);
	run();

</script>



</body></html>