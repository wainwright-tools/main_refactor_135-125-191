<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight dark:text-white">
            {{ __('Games List') }}
        </h2>
    </x-slot>
    <div class="max-w-7xl dark:bg-gray-900 mx-auto py-4 px-2 sm:px-6 lg:px-8">                

<div class="py-2 px-4 md:px-2 flex items-center justify-center grid grid-cols-2 gap-6 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-4 xl:grid-cols-5">
    @foreach ($games_list['games'] as $game)
        <a href="{{ $game->link() }}">
            <div class="group relative bg-gray-300 hover:bg-gray-100 dark:hover:bg-gray-600 dark:bg-gray-700 rounded-2xl shadow-md cursor-pointer">
                <div class="mb-1">
                    <div class="image block w-full h-full items-center object-cover justify-center">
                        <div class="transition duration-300 ease absolute opacity-0 group-hover:opacity-100 z-10 items-center p-2"><button class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition ml-1">Play</button></div>    
                        <img class="object-cover rounded-t-2xl w-full opacity-100 group-hover:opacity-25" src="{{ $game->thumb($game->gid) }}" loading="lazy">
                    </div>
                    <div class="py-2 px-3"> 
                    <p class="text-sm font-semibold text-gray-800 dark:text-gray-100 my-1 game-name">{{$game->name}}</p>
                    <div class="flex space-x-2 text-gray-400 text-xs">
                        <svg class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                        </svg>
                    <p>{{$game->provider}}</p>
                    </div>
                </div>
                </div>
            </div>
        </a>
    @endforeach
    <div class="container-fluid mx-auto mt-10 mb-20" style="max-width:1250px;">
        {!! $games_list['games']->links('respins::partials.pagination') !!}		
    </div>
</div>
</div>
