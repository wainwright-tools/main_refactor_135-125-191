<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight dark:text-white">
            {{ __('Game Launcher') }}
        </h2>
    </x-slot>
@if($error === 1)
  <div class="container mx-auto px-4 py-24">
        @if (session()->has('error'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 rounded relative" role="alert">
              <strong class="font-bold">Something weird happened..</strong>
              <span class="block sm:inline">{{ session('error') }}</span>
            </div>
             <a href="/game-api/" class="hover:bg-gray-100 text-gray-100 hover:text-gray-800 font-medium text-xs py-2 px-4 border border-gray-400 rounded-full shadow cursor-pointer">Gameslist</a>
             <a class="hover:bg-gray-100 text-gray-100 hover:text-gray-800 font-medium text-xs py-2 px-4 border border-gray-400 rounded-full shadow cursor-pointer" href="/full-whitelabel-crypto-casino-solution">Full Fledged Solution</a>
        @endif
    </div>
@else
    <div class="container mx-auto center-items justify-center mt-8" style="max-width: 1250px;">
        <div class="py-2">
            <div class="mx-auto sm:px-6 lg:px-8">
            <div class="aspect-w-16 aspect-h-9 rounded-lg">
                  <iframe style="max-height: 650px;" src="{{$url}}" class="w-full h-screen rounded-lg" frameborder="0"></iframe>
            </div>
            <div class="mt-2">
                <x-jet-button wire:click="$emit('refreshComponent')" class="ml-4">
                    Refresh
                </x-jet-button>

                <!-- Session Url -->
                <a href="{{ $url }}" target="_blank" class="inline-flex items-center text-xs bg-gray-700 text-gray-400 border-0 transition ease duration-300 mb-4 py-2 px-4 focus:outline-none hover:bg-gray-800 hover:text-gray-100 rounded-full mt-4 md:mt-0">
                    Session Entry Url: {{ $url }}
                </a>
            </div>
        </div>
        </div>
        <div class="mt-10"></div>
        <x-jet-action-section>
            <x-slot name="title">
                <span class="dark:text-white">{{ __('Game Actions') }}</span>
            </x-slot>
            <x-slot name="description">
                {{ __('Simple Game Actions') }}
            </x-slot>
            <x-slot name="content" class="dark:bg-gray-700">
                <div class="max-w-xl text-sm text-gray-600">
                    {{ __('Actions apply to your current loaded game session.') }}
                </div>
                <div class="mt-5">
                <div class="flex items-center mt-5">
                    <x-jet-button class="ml-1" wire:click="send_win" wire:loading.attr="disabled">
                        {{ __('Send Win') }}
                    </x-jet-button>
                </div>
                </div>
            </x-slot>
        </x-jet-action-section>

        <div class="mt-10 mb-10"></div>

        <livewire:gamesessions-datatable />
        
    </div>
@endif

