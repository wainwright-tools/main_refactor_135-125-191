<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight dark:text-white">
            {{ __('Operator Panel') }}
        </h2>
    </x-slot>
        <div class="max-w-7xl dark:bg-gray-900 dark:text-white mx-auto py-10 sm:px-6 lg:px-8">   
        <div class="mt-10 mb-10"></div>
        <x-jet-form-section submit="generateKey">
            <x-slot name="title">
                    <span class="dark:text-white">{{ __('Generate API Key') }}</span>
            </x-slot>
            <x-slot name="description">
                {{ __('Generate a new API key (tied to your user).') }}
            </x-slot>
            <x-slot name="form">
                <div class="col-span-6 sm:col-span-4">
                <div class="max-w-xl mb-4 text-sm text-gray-600">
                    <p class="mb-2">{{ __('Generate new API key.') }}</p>
                    <p>{{ __('You can also import the gamelist manually first and save it as a .json file, then upload it somewhere and paste direct link to the .json file below.') }}</p>
                </div>
                    <x-jet-label for="ip" value="{{ __('Enter Access IP (ipv4 only):') }}" />
                    <x-jet-input id="ip" placeholder="{{ $request_ip }}" type="text" class="mt-1 text-gray-800 block w-full" wire:model.defer="state.ip" />
                    <x-jet-input-error for="ip" class="mt-2" />
                    <x-jet-label class="mt-2" for="callback_url" value="{{ __('Callback URL') }}" />
                    <x-jet-input id="callback_url" placeholder="{{ env('APP_URL') }}/api/respins.io/games/callback" type="text" class="mt-1 text-gray-800 block w-full" wire:model.defer="state.callback_url" />
                    <x-jet-input-error for="callback_url" class="mt-2" />
                </div>
            </x-slot>

            <x-slot name="actions">
                <x-jet-action-message class="mr-3" on="generatedKey">
                    Created key, refresh to see.
                </x-jet-action-message>
                <x-jet-button wire:loading.attr="disabled">
                    {{ __('Generate New Key') }}
                 </x-jet-button>
            </x-slot>
            </x-jet-form-section>
        <div class="mt-10 mb-10"></div>

        <a href="{{ env('APP_URL') }}/api/respins.io/aggregation/createSession?game=softswiss:AlohaKingElvis&currency=USD&mode=real&player=dejan&operator_key=96e9f726-74e3-4752-a0ca-7bcc758d7f43" target="_blank" class="inline-flex items-center text-xs bg-gray-700 text-gray-400 border-0 transition ease duration-300 mb-4 py-2 px-4 focus:outline-none hover:bg-gray-800 hover:text-gray-100 rounded-full mt-4 md:mt-0">
                  <p>Example (replace game, operator key accordingly):</p>
                  <br><br>
                  <p><i>/api/respins.io/aggregation/createSession?game=softswiss:AlohaKingElvis&currency=USD&mode=real&player=dejan&operator_key=96e9f726-74e3-4752-a0ca-7bcc758d7f43</i></p>
            </a>

            <livewire:user-datatable />
        <div class="mt-10 mb-10"></div>
            <livewire:operatorkeys-datatable />
        </div>
    </div>