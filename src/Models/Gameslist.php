<?php

namespace Respins\BaseFunctions\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;
use DB;
class Gameslist extends Eloquent  {
    protected $table = 'respins_gameslist';
    protected $timestamp = true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'gid',
        'slug',
        'name',
        'provider',
        'type',
        'typeRating',
        'popularity',
        'bonusbuy',
        'jackpot',
        'demoplay',
        'demolink',
        'origin_demolink',
        'source',
        'realmoney',
        'method',
        'enabled',
        'created_at',
        'updated_at'
    ];  
    protected $casts = [
        'enabled' => 'boolean',
        'realmoney' => 'json',
        'rawobject' => 'json'
    ];

    public function link(){
        $url = config('baseconfig.frontend.launcher_url') ?? 'http://localhost';
        $path = config('baseconfig.frontend.launcher_path') ?? 'play-game';
        $complete_link = $url.'/'.$path.'/'.$this->slug;
        return $complete_link;
    }
    public static function static_link($game_slug){
        $url = env('APP_URL').'/web/respins.io/games/launch/'.$game_slug;
        return $url;
    }

    public static function collectify_davidkohen_list()
    {
        $get = collect(file_get_contents('../../davidkohen_gamelist.json'));
        return $get;
    }


    public function thumb($game_id)
    {
        $complete_link = config('gameconfig.thumbnail_square').$game_id.'.png';
        return $complete_link;
    }

    public static function thumbnail_square($game_id){
        $complete_link = config('gameconfig.thumbnail_square').$game_id.'.png';
        return $complete_link;
    }

    public static function thumbnail_wide($game_id) {
        $complete_link = config('gameconfig.thumbnail_wide').$game_id.'.png';
        return $complete_link;
    }


    public static function build_list(){
        $query = DB::table('respins_gameslist')->distinct()->get('provider');

        $providers_array[] = array();
        foreach($query as $provider) {
            $provider_array[] = array(
                'slug' => $provider->provider,
                'provider' => $provider->provider,
                'name' => ucfirst($provider->provider),
                'methods' => 'demoModding',
            );
        }

        return json_encode($provider_array, true);
    }
    

    public static function Providers(){
        $query = DB::table('respins_gameslist')->distinct()->get('provider');

        $providers_array[] = array();
        foreach($query as $provider) {
            $provider_array[] = array(
                'slug' => $provider->provider,
                'provider' => $provider->provider,
                'name' => ucfirst($provider->provider),
                'methods' => 'demoModding',
            );
        }

        return json_encode($provider_array, true);
    }
    
    
}

