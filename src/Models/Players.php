<?php

namespace Respins\BaseFunctions\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Players extends Eloquent  {
      
    protected $table = 'respins_players';
    public $timestamps = true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'player_id',//aggregator side uuid (main identifier)
        'player_operator_id',//operator player id
        'operator_key',
        'nickname',
        'currency',
        'ownedBy',
    ];

    protected $casts = [
        'data' => 'json',
        'active' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(\Respins\BaseFunctions\Models\OperatorAccess);
    } 
} 