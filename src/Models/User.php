<?php

namespace Respins\BaseFunctions\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class User extends \App\User
{
    public function operatoraccess(){
        return $this->belongsToMany('Respins\BaseFunctions\Models\OperatorAccess', 'ownedBy');
    }

    public function player(){
        return $this->belongsToMany('Respins\BaseFunctions\Models\Players', 'ownedBy');
    }

} 