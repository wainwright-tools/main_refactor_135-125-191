<?php
namespace Respins\BaseFunctions\Controllers\Data;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use DB;
use Respins\BaseFunctions\BaseFunctions;
use Respins\BaseFunctions\Traits\ApiResponseHelper;
use Respins\BaseFunctions\Models\RawGameslist;
use Respins\BaseFunctions\Models\Gameslist;
use Respins\BaseFunctions\Jobs\RetrieveDemolinkJob;

class GameImporterController
{
    use ApiResponseHelper;

    public static function curl($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $resp = curl_exec($curl);
        curl_close($curl);
        return json_decode($resp, true);
    }

    public static function game_importer_transfer()
    {
        $getRaw = RawGameslist::all();
        foreach ($getRaw as $game) {
            $prepareArray = [
                'gid' => $game->gid,
                'slug' => $game->slug,
                'name' => $game->name,
                'provider' => $game->provider,
                'type' => $game->type,
                'typeRating' => $game->typeRating,
                'popularity' => $game->popularity,
                'bonusbuy' => $game->bonusbuy,
                'jackpot' => $game->jackpot,
                'demoplay' => $game->demoplay,
                'origin_demolink' => $game->origin_demolink,
                'source' => $game->source,
                'realmoney' => json_encode($game->realmoney),
                'method' => 'demo_method',
                'enabled' => 1,
            ];           
            Gameslist::insert($prepareArray);
        }
    }
    
    public static function game_importer_livewire($url, $filterkey = NULL, $filtervalue = NULL)
    {
        //$url = 'https://gitlab.freedesktop.org/ryan-gate-2/casino-montik/-/raw/main/games255__2_.json';
        $url = urldecode($url);
        $parse = parse_url($url);
        $originTarget = preg_replace('/^www\./', '', $parse['host']);

        $getGames = self::curl($url);
        if($getGames === false) {
            $message = array('status' => 'error', 'url' => $url, 'message' => 'URL did not respond with status 200, indicating our request is incorrect or server IP is geo-blocked.');
            return json_encode($message);
        }

        if($getGames === NULL) {
            $message = array('status' => 'error', 'url' => $url, 'message' => 'Response seemed completely empty.');
            return json_encode($message);
        }

        $count_before = RawGameslist::count();
        foreach ($getGames as $gameID => $data) {
            $explodeSSid = explode('/', $gameID);
            $bindTogether = $explodeSSid[0].':'.$explodeSSid[1];
            $typeGame = 'casino';
            $demoMode = 0;
            $demoPrefix = 0;
            $typeRatingGame = 0;
            $internal_origin_realmoneylink = [];

            if(isset($data['demo'])) {
                $demoMode = true;
                $demoPrefix = urldecode($data['demo']);
                if($originTarget === 'bitstarz.com') {
                    $demoPrefix = str_replace('http://bitstarz.com', '', $demoPrefix);
                }
            }

            if(isset($data['real'])) {
                $internal_origin_realmoneylink = $data['real'];        
            }

            $stringifyDetails = json_encode($data['collections']);        
            if(str_contains($stringifyDetails, 'slots')) {
                $typeGame = 'slots';
                if(isset($data['collections']['slots'])) {
                    $typeRatingGame = $data['collections']['slots'];
                } else {
                    $typeRatingGame = 100;
                }
            }

            if(str_contains($stringifyDetails, 'live')) {
                $typeGame = 'live';
                if(isset($data['collections']['live'])) {
                    $typeRatingGame = $data['collections']['live'];
                } else {
                    $typeRatingGame = 100;
                }
            }

            if(str_contains($stringifyDetails, 'bonusbuy')) {
                $hasBonusBuy = 1;
            } else {
                $hasBonusBuy = 0;
            }

            if(str_contains($stringifyDetails, 'jackpot')) {
                $hasJackpot = 1;
            } else {
                $hasJackpot = 0;
            }

            $prepareArray = array(
                'gid' => $gameID,
                'slug' => $bindTogether,
                'name' => $data['title'],
                'provider' => $data['provider'],
                'type' => $typeGame,
                'typeRating' => $typeRatingGame,
                'popularity' => $data['collections']['popularity'],
                'bonusbuy' => $hasBonusBuy,
                'jackpot' => $hasJackpot,
                'demoplay' => $demoMode,
                'origin_demolink' => $demoPrefix,
                'source' => $originTarget,
                'realmoney' => json_encode($internal_origin_realmoneylink),
                'rawobject' => json_encode($data),
                'mark_transfer' => 0,
            );

            if($filterkey !== NULL && $filtervalue !== NULL) {
                if($prepareArray[$filterkey] === $filtervalue) {
                    RawGameslist::insert($prepareArray);
                }
            } else {
                RawGameslist::insert($prepareArray);
            }
        }
        $count_after = RawGameslist::count();
        $final_count = $count_after - $count_before;

        $message = array('status' => 'success', 'message' => 'Succesfully imported '.$final_count.' games to raw gameslist.', 'count_games_imported' => $final_count);
        return json_encode($message);
    }
}