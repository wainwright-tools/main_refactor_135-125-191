<?php

namespace Respins\BaseFunctions\Controllers\Livewire\Partials;

use App\Models\User;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class UserDataTable extends DataTableComponent
{
    protected $model = User::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make('Email', 'email')
                ->sortable(),
            Column::make('Name')
                ->sortable(),
        ];
    }
}