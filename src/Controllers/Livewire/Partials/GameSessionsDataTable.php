<?php

namespace Respins\BaseFunctions\Controllers\Livewire\Partials;

use \Respins\BaseFunctions\Models\GameSessions;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class GameSessionsDataTable extends DataTableComponent
{
    protected $model = GameSessions::class;
    public function configure(): void
    {
        $this->setPerPageAccepted([25, 50, 100, 200]);
        $this->setPrimaryKey('id');
        $this->setDefaultSort('id', 'desc');
    }

    public function columns(): array
    {
        return [
            Column::make('Player ID', 'player_id')
                ->sortable(),
            Column::make('Game', 'game_id')
                ->sortable(),
            Column::make('State', 'state')
                ->sortable(),
            Column::make('Internal Token', 'token_internal')
                ->sortable(),
            Column::make('Token Original', 'token_original')
                ->sortable(),
        ];
    }
}