<?php
namespace Respins\BaseFunctions\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Respins\BaseFunctions\Controllers\Data\DataJobFunctions;
class RetrieveRealDemoURL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $game;

    public function __construct(string $game)
    {
        $this->game = $game;
    }

    public function handle()
    {
        $data = $this->game;
        return DataJobFunctions::retrieve_demolink($data);
    }
}
